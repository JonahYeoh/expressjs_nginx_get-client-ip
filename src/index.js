/***
 * I prefer import over require,
 * May switch it to require by remove "type" from package.json
 */
import express from "express";
import cors from "cors";

var app = express();
var router = express.Router();

router.use(cors({origin: "*"}));

router.get("/MyIP", async (req, res) => {
    res.json({
        x_real_ip: req.header('X-Real-IP'),
        remoteAddress: req.socket.remoteAddress,
        x_forwareded_for: req.headers["x-forwarded-for"]
    });
});

app.set("title", "MyIP");
app.set("trush proxy", true); // important
app.use(router);

app.listen(3000, "127.0.0.1", () => {
    console.log("listening...");
});