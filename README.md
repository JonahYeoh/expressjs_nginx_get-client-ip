# Nginx Configuration
```
nginx -V
nginx version version: nginx/1.18.9 (Ubuntu)
built with OpenSSL 1.1.1f 31 mar 2020
TLS SNI support enabled
...
```
# Steps:

1. ## filepath: /etc/nginx/sites-avaialble/domain
    ```
        upstream backend {
            server 127.0.0.1:3000;
        }

        server {
            server_name domain;

            location /MyIP {
                proxy_set_header X-Real-IP $remote_addr; # This is important!!!
                proxy_pass http://backend;
            }

            # Configuration below are automatically added by letsnecrypt, so copy it yourself
            # listen 443 ssl http2; # managed by Certbot, http2 were manually added
            # ssl_certificate /etc/letsencrypt/live/domain/fullchain.pem; # managed by Certbot
            # ssl_certificate_key /etc/letsencrypt/live/domain/privkey.pem; # managed by Certbot
            # include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
            # ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
        }

        server {
            if ($host = domain) {
                return 301 https://$host$request_uri;
            } # managed by Certbot

            server_name domain;
            listen 80;
            return 404; # managed by Certbot
        }
    ```

    2. `sudo nginx -t` to check whether the configuration is compliance with Nginx format

    3. `sudo systemctl restart nginx` to restart the Nginx service

    4. At this repo `node /src/index.js` to run ExpressJS server